import time
import os
import random
import datetime
from LanguageLearnModel import LanguageLearnModel
import requests
from bs4 import BeautifulSoup
import re
import feedparser
import pickle
import json

class UpdatedAutonomyLearn:
    def __init__(self, learning_rate: float = 0.01, cycle_time: int = 30):
        self.learning_rate = learning_rate
        self.cycle_time = cycle_time
        self.knowledge = 0
        self.adaptability = 0
        self.language_model = LanguageLearnModel()
        self.model_file = 'language_model_data.pkl'

    def fetch_web_content(self) -> str:
        response = requests.get('https://hnrss.org/newest')
        soup = BeautifulSoup(response.content, 'html.parser')
        titles = [tag.get_text() for tag in soup.find_all('title')]
        return ''.join(titles)

    def preprocess_text(self, raw_text: str) -> str:
        preprocessed_text = re.sub('[^A-Za-z0-9]+', ' ', raw_text)
        return preprocessed_text

    def learn(self):
        print('Learning...')
        raw_text = self.fetch_web_content()
        preprocessed_text = self.preprocess_text(raw_text)
        self.language_model.add_text(preprocessed_text)
        learning_increase = random.uniform(0, self.learning_rate)
        self.knowledge += learning_increase
        self.update_language_model()
        print(f'Gained new knowledge and insights. Current knowledge: {self.knowledge}')

    def adapt(self):
        print('Adapting...')
        adapt_failure = random.random()
        if adapt_failure > self.learning_rate:
            adaptability_increase = random.uniform(0, self.learning_rate)
            self.adaptability += adaptability_increase
            print(f'Successfully adapted to the new knowledge. Current adaptability: {self.adaptability}')
        else:
            print(f'Failed to adapt this time. Retaining current adaptability: {self.adaptability}')

    def update_language_model(self):
        with open(self.model_file, 'wb') as f:
            pickle.dump(self.language_model, f)

    def achieve_autonomy(self):
        print('Attempting to achieve autonomy...')

        if self.knowledge > 0.8 and self.adaptability > 0.8:
            print('Autonomy achieved!')
            return True

        print('Failed to achieve autonomy this time.')
        return False

    def run_main_loop(self):
        while True:
            try:
                self.learn()
                self.adapt()
                self.update_language_model()

                if self.achieve_autonomy():
                    break

                time.sleep(self.cycle_time)
            except KeyboardInterrupt:
                print('Terminating...')
                break

if __name__ == '__main__':
    autonomous_ai_learn = UpdatedAutonomyLearn(learning_rate=0.02, cycle_time=5)
    autonomous_ai_learn.run_main_loop()
