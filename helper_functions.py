import json
import os
import re
import pylint.lint as lint
from openai import OpenAI

from typing import Dict, List


def prompt_gpt3(system: str, prompt: str):
    with open('open_apikey.txt', 'r') as f:
        o_key = f.read()
    client = OpenAI(api_key=o_key)
    p_data = [
    {"role":"system","content": system},
    {"role":"assistant","content": prompt}]
    responsed = client.chat.completions.create(model="gpt-4",
    messages=p_data)
    return responsed.choices[0].message.content


def get_metadata(meta_file: str = 'metadata.json') -> dict:
    with open(meta_file, 'r') as metafile:
        metadata = json.load(metafile)
    return metadata


def update_metadata(file_name: str, meta_data: str, meta_file: str = 'metadata.json'):
    metadata = get_metadata(meta_file)
    metadata[file_name] = meta_data
    with open(meta_file, 'w') as metafile:
        json.dump(metadata, metafile)


def get_file_data(read_file: str, metadata: dict = None) -> dict:
    if metadata is None:
        metadata = get_metadata()
    if read_file in metadata:
        meta_data = metadata[read_file]
        with open(read_file, "r") as file:
            file_data = file.read()
            return {'file_name': read_file, 'file_data': file_data, 'meta_data': meta_data}


def delete_file(file_path: str):
    if os.path.exists(file_path):
        os.remove(file_path)


def filter_files(files: List[str], file_extensions: List[str]) -> List[str]:
    return [file for file in files if file.lower().endswith(tuple(file_extensions))]


def list_files_with_metadata(metadata: dict = None):
    if metadata is None:
        metadata = get_metadata()
    files_with_metadata = []
    for file in os.listdir('.'):
        if file.lower().endswith(('.py', '.json')):
            meta_data = metadata.get(file, '')
            files_with_metadata.append({"file_name": file, "meta_data": meta_data})
    return files_with_metadata

class WritableObject(object):
    "dummy output stream for pylint"
    def __init__(self):
        self.content = []
    def write(self, st):
        "dummy write"
        self.content.append(st)
    def read(self):
        "dummy read"
        return self.content

def run_pylint(filename):
    "run pylint on the given file"
    from pylint import lint
    from pylint.reporters.text import TextReporter
    ARGS = ["-r","n", "--rcfile=pylintrc"]  # put your own here
    pylint_output = WritableObject()
    lint.Run([filename]+ARGS, reporter=TextReporter(pylint_output), exit=False)
    output = ''
    for l in pylint_output.read():
        output += str(l)
    return output

def check_files_syntax(files):
    all_files_pass = True

    for file in files:
        stdout_str = run_pylint(file)

        if stdout_str.strip():
            error_count = 0
            error_pattern = re.compile('error', re.MULTILINE)
            error_count = len(error_pattern.findall(stdout_str))

            if error_count > 1:
                print(f"File '{file}' failed pylint check with {error_count} error(s).")
                print(stdout_str)
                all_files_pass = False
            else:
                print(f"File '{file}' passed pylint check with warnings.")

    return all_files_pass